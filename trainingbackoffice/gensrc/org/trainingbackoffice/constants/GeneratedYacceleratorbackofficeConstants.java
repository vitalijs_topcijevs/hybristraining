/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Apr 25, 2021 11:41:53 PM                    ---
 * ----------------------------------------------------------------
 */
package org.trainingbackoffice.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedYacceleratorbackofficeConstants
{
	public static final String EXTENSIONNAME = "trainingbackoffice";
	
	protected GeneratedYacceleratorbackofficeConstants()
	{
		// private constructor
	}
	
	
}
