/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Apr 25, 2021 11:41:53 PM                    ---
 * ----------------------------------------------------------------
 */
package org.training.core.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedTrainingCoreConstants
{
	public static final String EXTENSIONNAME = "trainingcore";
	public static class TC
	{
		public static final String APPARELPRODUCT = "ApparelProduct".intern();
		public static final String APPARELSIZEVARIANTPRODUCT = "ApparelSizeVariantProduct".intern();
		public static final String APPARELSTYLEVARIANTPRODUCT = "ApparelStyleVariantProduct".intern();
		public static final String ECENTANOTIFICATION = "EcentaNotification".intern();
		public static final String ECENTANOTIFICATIONCRONJOB = "EcentaNotificationCronJob".intern();
		public static final String ELECTRONICSCOLORVARIANTPRODUCT = "ElectronicsColorVariantProduct".intern();
		public static final String NOTIFICATIONPRIORITYENUM = "NotificationPriorityEnum".intern();
		public static final String NOTIFICATIONTYPEENUM = "NotificationTypeEnum".intern();
		public static final String SWATCHCOLORENUM = "SwatchColorEnum".intern();
	}
	public static class Attributes
	{
		public static class Order
		{
			public static final String ECENTANOTIFICATION = "ecentaNotification".intern();
		}
	}
	public static class Enumerations
	{
		public static class NotificationPriorityEnum
		{
			public static final String LOW = "Low".intern();
			public static final String NORMAL = "Normal".intern();
			public static final String HIGH = "High".intern();
		}
		public static class NotificationTypeEnum
		{
			public static final String ORDERMANAGEMENT = "OrderManagement".intern();
			public static final String NEWS = "News".intern();
			public static final String SERVICETICKETS = "ServiceTickets".intern();
			public static final String WORKFLOW = "WorkFlow".intern();
		}
		public static class SwatchColorEnum
		{
			public static final String BLACK = "BLACK".intern();
			public static final String BLUE = "BLUE".intern();
			public static final String BROWN = "BROWN".intern();
			public static final String GREEN = "GREEN".intern();
			public static final String GREY = "GREY".intern();
			public static final String ORANGE = "ORANGE".intern();
			public static final String PINK = "PINK".intern();
			public static final String PURPLE = "PURPLE".intern();
			public static final String RED = "RED".intern();
			public static final String SILVER = "SILVER".intern();
			public static final String WHITE = "WHITE".intern();
			public static final String YELLOW = "YELLOW".intern();
		}
	}
	public static class Relations
	{
		public static final String ECENTANOTIFICATION2ORDERS = "EcentaNotification2Orders".intern();
	}
	
	protected GeneratedTrainingCoreConstants()
	{
		// private constructor
	}
	
	
}
