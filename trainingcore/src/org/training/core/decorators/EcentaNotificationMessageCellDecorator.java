package org.training.core.decorators;

import de.hybris.platform.util.CSVCellDecorator;

import java.util.Map;

public class EcentaNotificationMessageCellDecorator implements CSVCellDecorator {
    @Override
    public String decorate(int i, Map<Integer, String> srcLine) {
        String parsedValue=srcLine.get(i);
        return parsedValue+"Sample data";
    }
}
