/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Apr 25, 2021 11:41:53 PM                    ---
 * ----------------------------------------------------------------
 */
package org.training.facades.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedTrainingFacadesConstants
{
	public static final String EXTENSIONNAME = "trainingfacades";
	
	protected GeneratedTrainingFacadesConstants()
	{
		// private constructor
	}
	
	
}
