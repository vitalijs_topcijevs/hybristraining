<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<style>
	div #originalText, #showLess {
		display: none;
	}
</style>

<div class="tabhead">
	<a href="">${fn:escapeXml(title)}</a> <span class="glyphicon"></span>
</div>
<div class="tabbody">
	<div class="container-lg">
		<div class="row">
			<div class="col-md-6 col-lg-4">
				<div class="tab-container">
				    <c:choose>
                		<c:when test="${fn:length(product.description) < 61}">
                			<product:productDetailsTab product="${product}" />
                		</c:when>
                		<c:otherwise>
                			<div id="shortText">${fn:substring(product.description, 0, 60)}...</div>
                			<div id="originalText"><product:productDetailsTab product="${product}" /></div>
                			<button id="showMore" onclick="toggleText()">Show More</button>
                			<button id="showLess" onclick="toggleText()">Show Less</button>
                	    </c:otherwise>
                	</c:choose>

					<script>
						function toggleText() {
							if ($("#originalText").is(":hidden")) {
								$("#originalText").show();
								$("#shortText").hide();
								$("#showMore").hide();
								$("#showLess").show();
							} else {
								$("#originalText").hide();
								$("#shortText").show();
								$("#showLess").hide();
								$("#showMore").show();
							}
						}
					</script>
				</div>
			</div>
		</div>
	</div>
</div>

