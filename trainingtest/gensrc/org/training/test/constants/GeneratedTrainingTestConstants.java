/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Apr 25, 2021 11:41:53 PM                    ---
 * ----------------------------------------------------------------
 */
package org.training.test.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedTrainingTestConstants
{
	public static final String EXTENSIONNAME = "trainingtest";
	
	protected GeneratedTrainingTestConstants()
	{
		// private constructor
	}
	
	
}
